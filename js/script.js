let userFirstNumber = +prompt('Please enter a first number', 20);
let userSecondNumber = +prompt('Please enter a second number', 10);

while (!userFirstNumber || !userSecondNumber || isNaN(userFirstNumber) || isNaN(userSecondNumber)) {
    userFirstNumber = +prompt('Please enter a first number', '');
    userSecondNumber = +prompt('Please enter a second number', '');
}



let mathOperator = prompt('Please enter one of the following math operators: "+", "-", "*" or "/"', '+');

function getResult(userFirstNumber, userSecondNumber) {

    let result = 0;

    switch (mathOperator) {

        case '+':
            result = userFirstNumber + userSecondNumber;
            break;

        case '-':
            result = userFirstNumber - userSecondNumber;
            break;

        case '*':
            result = userFirstNumber * userSecondNumber;
            break;

        default:
            result = userFirstNumber / userSecondNumber;
            break;
    }

    return result;
}

console.log(getResult(userFirstNumber, userSecondNumber));